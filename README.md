[![LABSOLAR](http://www.lepten.ufsc.br/figuras/labsolar%20-%20pequeno.jpg)](https://www.lepten.ufsc.br/)
========

Especificamente na área solar, o LEPTEN/Labsolar tem concentrado esforços em duas frentes. A primeira delas caracteriza-se pela implementação de dados de irradiação confiáveis e contínuos a partir de cinco estações no estado de Santa Catarina e uma no Amazonas. A segunda frente caracteriza-se pelo desenvolvimento, em parceria com o INPE (Instituto Nacional de Pesquisas Espaciais), de modelos computacionais capazes de determinar a intensidade da radiação solar incidente na superfície, a partir de imagens de satélite. Como resultado, destaca-se a publicação, com apoio do INMET, da primeira versão do "Atlas de Irradiação Solar do Brasil" <https://www.lepten.ufsc.br/pesquisa/solar/atlas_de_irradiacao.pdf> em versão impressa e CD multimídia.

O laboratório desenvolve ainda, pesquisas em coletores solares compactos acoplados a reservatório e sistemas de ar condicionado auxiliados por energia solar. Outra linha de pesquisa é a análise do desempenho de instalações fotovoltaicas autônomas, para locais remotos, ou interligadas à rede elétrica.

O laboratório gerencia duas estações de superfície no contexto do projeto BSRN (Baseline Surface Radiation Network) <http://bsrn.awi.de/> / WMO (World Meteorological Organization) <http://www.wmo.int/>, localizadas em Florianópolis, nas dependências do Laboratório e na Usina Hidrelétrica de Balbina / AM.

Tais estações fazem parte de uma rede internacional de estações de monitoração de clima, para fins de coletas de dados para subsidiar o desenvolvimento de modelos físicos de comportamento da atmosfera. Os dados coletados e qualificados são armazenados no WMRC (World Monitoring Radiation Center) <http://bsrn.awi.de/>, em Zurique. A rede é considerada internacionalmente como referência de qualidade em coleta de dados para fins de estudos científicos de mudanças climáticas.

Além dos laboratórios instalados na UFSC, o LEPTEN/Labsolar possui ainda uma base física de testes na SE Campos Novos, pertencente à ELETROSUL.

Infraestrutura
========

- central fotovoltaica interligada a rede de distribuição;
- bancada de teste de coletores solares para aquecimento de água;
- central solar de aquecimento de água;
- criostatos;
- máquina de solda-ponto;
- detector de vazamentos;
- bomba de vácuo difusora;
- bomba de vácuo turbomolecular;
- torre de resfriamento;
- circuito interno de ar comprimido;
- fontes de alimentação;
- banho ultrasônico;
- sistema de calibração de termopares;
- bancada para confecção de termopares;
- sistemas de aquisição de dados;
- pirheliômetro;
- multímetros.
- rede com 40 microcomputadores PC conectados à Internet;
- impressoras laser e jato de tinta;
- servidor de rede;
- cluster com 12 Ghz para simulação térmica utilizando o software CFX;
- cluster com 35,6 Ghz e 15 Gb de memória para simulação climática utilizando o software ARPS. 
